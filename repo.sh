#!/bin/bash

# Install nginx and createrepo
sudo apt-get update
sudo apt-get install -y nginx createrepo-c certbot python3-certbot-nginx 
# Set up Nginx config

sudo rm /etc/nginx/sites-enabled/default

sudo tee /etc/nginx/sites-available/repo <<EOF

server {
    listen 80;
    server_name repo.neuronmaster.xyz;

    location / {
        autoindex on;
        root /var/www/html/repo;
    }
}

EOF

sudo ln -s /etc/nginx/sites-available/repo /etc/nginx/sites-enabled/repo

sudo systemctl enable --now nginx

# Obtain SSL certificate using Certbot
sudo certbot --nginx -d repo.neuronmaster.xyz 

# Configure automatic certificate renewal
sudo certbot renew --dry-run



# Reload nginx configuration
sudo systemctl reload nginx


sudo systemctl restart nginx

# Add some packages to the repo - on practice did it with scp from another machine where i've built RPM
sudo cp *.rpm /var/www/html/repo/

# Create the repo
sudo mkdir -p /var/www/html/repo
cd /var/www/html/repo
sudo createrepo_c .


